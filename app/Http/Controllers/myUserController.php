<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\myUser;
use Hash;


class myUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $myUsers = myUser::all();

        return view('welcome', compact("myUsers"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'first_name' => 'required|string|min:3|max:20',
            'last_name'  => 'required|string|min:3|max:20',
            'email'      => 'required|string|email|unique:myUsers|max:225',
            'phone'      => 'nullable|min:13|max:13',
            'password'   => 'required|string|min:8|max:18'
            
        ]);

        $newUser = new myUser();
        $newUser->first_name = $request->first_name;
        $newUser->last_name  = $request->last_name;
        $newUser->email      = $request->email;
        $newUser->phone      = $request->phone;
        $newUser->password   = Hash::make($request->password);
        $newUser->save();
      
        return redirect()->back()->with("msg", "New User ".$request->first_name." added to database!");
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $UserDetails = myUser::find($id);

        return view('updateUser', compact('UserDetails'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'first_name' => 'required|string|min:3|max:20',
            'last_name'  => 'required|string|min:3|max:20',
            'email'      => 'required|string|email|unique:myUsers|max:225',
            'phone'      => 'nullable|min:13|max:13',
            'password'   => 'required|string|min:8|max:18'
            
            ]);
            
        $UserDetails = myUser::find($id);
        $UserDetails->first_name = $request->first_name;
        $UserDetails->last_name  = $request->last_name;
        $UserDetails->email      = $request->email;
        $UserDetails->phone      = $request->phone;
        $UserDetails->password   = Hash::make($request->password);
        $UserDetails->save();
      
        return redirect('myUsers')->with("msg", "User ".$request->first_name." updated to database!");
        


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $UserDetails = myUser::find($id);
        $UserDetails->DELETE();
        return redirect()->back()->with("msg", "User ".$UserDetails->first_name." deleted to database!");
        

    }
}
