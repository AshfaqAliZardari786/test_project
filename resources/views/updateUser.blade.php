<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Update Users | Assignment Task</title>

        <!-- Fonts and CSS -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" href="{{asset('css/datatables.min.css')}}">
        
        <!-- Script -->
        <script src="{{asset('js/app.js')}}"></script>
        <script src="{{asset('js/datatables.min.js')}}"></script>
        <script src="{{asset('js/jquery.mask.js')}}"></script>
        
        
    </head>
    <body>
       
        <!-- Navbar Start -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-success">
            <a class="navbar-brand" href="#">Assignment Task</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                
                </ul> 
            </div>
        </nav>
        <!-- Navbar End -->

        <!-- Main Content Start -->
        <div class="container mt-5">            
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">

                <div class="card">
                    <div class="card-body">
                        
                        <h2 class="text-success text-center font-weight-bold">Assignment Task <small>( Update User )</small></h2>
                        
                        <form class="m-3" action="{{URL::to('myUsers/'.$UserDetails->id)}}" method="post">
                        @csrf
                        {{method_field('PUT')}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="first_name" class="col-form-label font-weight-bold">First Name</label>
                                        <input type="text" value="{{$UserDetails->first_name}}" class="form-control border-success" name="first_name" id="first_name" placeholder="">
                                        @error('first_name')
                                            <small class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>        
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="last_name" class="col-form-label font-weight-bold">Last Name</label>
                                        <input type="text" value="{{$UserDetails->last_name}}" class="form-control border-success" name="last_name" id="last_name" placeholder="">
                                        @error('last_name')
                                            <small class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-form-label font-weight-bold">Email</label>
                                <input type="text" value="{{$UserDetails->email}}" class="form-control border-success" name="email" id="email" placeholder="">
                                @error('email')
                                    <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="phone" class="col-form-label font-weight-bold">Phone </label>
                                <input type="text" data-mask="+000000000000" data-mask-selectonfocus="true" value="{{old('phone')}}" class="form-control border-success" name="phone" id="phone" placeholder="example: +923XXXXXXXXX ( Optional )">
                            @error('phone')
                                    <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        
                            <div class="form-group">
                                <label for="password" class="col-form-label font-weight-bold">Password</label>
                                <input type="password" class="form-control border-success" name="password" id="password" placeholder="">
                                @error('password')
                                    <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>

                            <div class="form-group">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success btn-block font-weight-bold" >Update User</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

                    
                </div>
                <div class="col-md-3"></div>
            </div> 

            <div class="row">
                <div class="col-md-3">
                    <a href="{{URL::to('myUsers')}}" class="btn btn-secondary">Go Back</a>
                </div>
                <div class="col-md-3"></div>
                <div class="col-md-3"></div>
            </div>              
        </div>
        <!-- Main Content End -->

    <!-- Footer Start -->
    <footer class="container-fluid mt-5 bg-success">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="text-center text-white pt-2">
                    CopyRight&copy;  <script>document.write(new Date().getFullYear());</script> <a href="{{URL::to('myUsers')}}" class="btn btn-link text-white">Assignment Task Inc.co</a>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
    </footer>
    <!-- Footer End -->

    <script>$('#myUsers').DataTable();$('input[name="phone"]').mask('+000000000000');</script>


    </body>
</html>
